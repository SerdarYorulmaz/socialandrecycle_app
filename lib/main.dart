import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'pages/home.dart';

void main(){
  Firestore.instance.settings(timestampsInSnapshotsEnabled: true).then(

      (_){
        print("Timestamps enabled in snapshots \n");
      },onError: (_){
    print("Error enabling timestamps in  snapshots \n");
  });

  runApp(MyApp());

}



class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Kullankazan',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.deepPurple,
          accentColor: Colors.teal,
        ),
        home: Home());
  }
}
