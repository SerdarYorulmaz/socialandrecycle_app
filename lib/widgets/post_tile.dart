import 'package:flutter/material.dart';
import 'package:free/pages/post_screen.dart';
import 'package:free/pages/profile.dart';
import 'package:free/widgets/custom_image.dart';
import 'package:free/widgets/post.dart';

class PostTile extends StatelessWidget {
  final Post post;

  PostTile(this.post); //contractor
  showPost(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PostScreen(
          postId: post.postId,
          userId: post.ownerId,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => showPost(context),
      child: cachedNetworkImage(post.mediaUrl),
    );
  }
}


