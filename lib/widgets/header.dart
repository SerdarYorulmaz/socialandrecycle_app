import 'package:flutter/material.dart';
import 'package:free/pages/home.dart';

AppBar header(context,
    {bool isAppTitle = false, String titleText, removeBackButton = false}) {
  return AppBar(
    automaticallyImplyLeading: removeBackButton ? false : true,
    title: Text(
      isAppTitle ? "kullantekrarkazan" : titleText,
      style: TextStyle(
        color: Colors.white,
        fontFamily: isAppTitle ? "Signatra" : "",
        fontSize: isAppTitle ? 50.0 : 22.0,
      ),
      overflow: TextOverflow.ellipsis,
    ),
    centerTitle: true,
    backgroundColor: Theme.of(context).accentColor,
//    actions:[
//
//         IconButton(
//            onPressed: () =>logout(context), //cikis
//            icon: Icon(
//              Icons.exit_to_app,
//              size: 30.0,
//              color: Colors.orange,
//            ),
//
//          ),
//
//    ],
  );
}
//logout(context) async{
//  await googleSignIn.signOut();
//  Navigator.push(context, MaterialPageRoute(builder: (context)=>Home()));
//}