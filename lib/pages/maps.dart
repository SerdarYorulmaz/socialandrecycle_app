import 'dart:async';
import 'package:flutter/material.dart';
import 'package:free/widgets/header.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Maps extends StatefulWidget {
  @override
  MapsState createState() => MapsState();
}

class MapsState extends State<Maps> {
  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    super.initState();
  }
  double zoomVal=5.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:header(context, titleText: "Kıyafet Noktaları"),
      body: Stack(
        children: <Widget>[
          _buildGoogleMap(context),
          _zoomminusfunction(),
          _zoomplusfunction(),
          _buildContainer(),
        ],
      ),
    );
  }

  Widget _zoomminusfunction() {

    return Align(
      alignment: Alignment.topLeft,
      child: IconButton(
          icon: Icon(FontAwesomeIcons.searchMinus,color:Colors.teal),
          onPressed: () {
            zoomVal--;
            _minus( zoomVal);
          }),
    );
  }
  Widget _zoomplusfunction() {

    return Align(
      alignment: Alignment.topRight,
      child: IconButton(
          icon: Icon(FontAwesomeIcons.searchPlus,color:Colors.teal),
          onPressed: () {
            zoomVal++;
            _plus(zoomVal);
          }),
    );
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(38.4962231, 27.7033611), zoom: zoomVal)));
  }
  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(38.4962231, 27.7033611), zoom: zoomVal)));
  }


  Widget _buildContainer() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        height: 150.0,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://lh5.googleusercontent.com/p/AF1QipO3VPL9m-b355xWeg4MXmOQTauFAEkavSluTtJU=w225-h160-k-no",
                  38.4905648, 27.7073549,"Alphart AVM"),
            ),

            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://simg.sputnik.ru/?key=74b7013a2e8fcd5cad8868c433f3a5a6d4d18a90",
                  38.4891819,27.7010217,"ucakTaksi"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "http://uk.keeptravel.com/resize/img/katalog/30/70/405811_3.jpg?h=160",
                  38.492658, 27.7028519,"Heykeller"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://im0-tub-tr.yandex.net/i?id=978680c12b6b6fb8f9da15594af01501&n=13&exp=1",
                  38.492227, 27.706980,"Universite"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://i.ebayimg.com/thumbs/images/g/FL8AAOSw9r1V~ILV/s-l225.jpg",
                  38.487024, 27.696922,"Misal Sitesi"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "http://newspile.ru/medias/news_images/2016/07/27/1469602884_1194916262.jpg",
                  38.486086, 27.703955,"Sağlık Ocağı"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://www.eveningnews24.co.uk/polopoly_fs/1.1676465!/image/3971544593.jpg_gen/derivatives/landscape_225/3971544593.jpg",
                  38.4838843, 27.7008687,"Irmak Sokak"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://im0-tub-tr.yandex.net/i?id=f72282e6516ab7a392c62ea7384bd1c1&n=13&exp=1",
                  38.4828146, 27.7012549,"Irlamaz Yolu"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://im0-tub-tr.yandex.net/i?id=bc911cf7a72964a0d81aadf089e5cd6f&n=13&exp=1",
                  38.488994, 27.701037,"Mimar Sinan Cad"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://simg.sputnik.ru/?key=fd301dd12dc9dc40cb748dde932664a9e96e529d",
                  38.489473, 27.726865,"Yunus Emre Cami"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "http://ucanbalonfiyat.com/organizasyon_resimler/goztepeucanbalon268.jpg",
                  38.490299, 27.728893,"Fatih Caddesi"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://www.russia-turismo.com/tourphoto/75t0.jpg",
                  38.487878, 27.725900,"Hüdavendigar"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "http://www.haldizoglukardeslerinsaat.com.tr/tr/onizleme.php?src=resimler/fotoresimleri/templatemo_banner_bg_02.jpg&w=225",
                  38.490771, 27.735469,"Bilgikent Okulları"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "http://uk.keeptravel.com/resize/img/katalog/30/111/440677_3.jpg?h=160",
                  38.490530, 27.73019,"Ergenekon Sağlık Ocağı"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://im0-tub-tr.yandex.net/i?id=7a7f7960f14fd608095a7bc081c6e06b&n=13&exp=1",
                  38.491953, 27.724832,"Arif Canpoyraz İlkokulu"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://lh5.googleusercontent.com/p/AF1QipMKRN-1zTYMUVPrH-CcKzfTo6Nai7wdL7D8PMkt=w340-h160-k-no",
                  38.486626, 27.698338,"Golden Towers"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://m.nb.cdn.bcdn.cz/categorySubcategories/905/905.jpg?1437938606",
                  38.496595, 27.706007,"Orta Park"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://www.westessexlife.co.uk/polopoly_fs/1.6046628.1557738199!/image/image.jpg_gen/derivatives/landscape_225/image.jpg",
                  38.492283, 27.72868 ,"Besime Elagöz Ortaokulu"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "http://newspile.ru/medias2/news_images/2017/05/03/1493775015_252390212.jpg",
                  38.49254, 27.728524,"Saklı Cennet"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://www.edp24.co.uk/polopoly_fs/1.1205494!/image/3529229202.jpg_gen/derivatives/landscape_225/3529229202.jpg",
                  38.494855, 27.724967,"Saray Park"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://pbs.twimg.com/media/DaG6Ez9UMAAgBb9.jpg",
                  38.50038, 27.7064471,"Dereköy Cami"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "http://newspile.ru/medias2/news_images/2017/03/18/1489811419_1627054494.jpg",
                  38.493992, 27.726831,"Turgutlu Ketenci Sitesi"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://www.suhakki.org/wp-content/uploads/2014/02/ortakoy-camii-iklim-degisikliginden-etkilenecek-225x160.jpg",
                  38.495367, 27.718504,"Şehitler Cami"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://im0-tub-tr.yandex.net/i?id=5ff560116f32535ea2083c62dfd0ef5b&n=13&exp=1",
                  38.4967, 27.711737,"Yalçın Sokak"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://im0-tub-tr.yandex.net/i?id=799544b122fe43c433b400acb4cc6559&n=13&exp=1",
                  38.500042, 27.709743,"Sedat Özcan Öğrenci Yurdu"),
            ),
          ],
        ),
      ),
    );
  }

  Widget _boxes(String _image, double lat,double long,String restaurantName) {
    return  GestureDetector(
      onTap: () {
        _gotoLocation(lat,long);
      },
      child:Container(
        child: new FittedBox(
          child: Material(
              color: Colors.white,
              elevation: 14.0,
              borderRadius: BorderRadius.circular(24.0),
              shadowColor: Color(0x802196F3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 180,
                    height: 200,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(24.0),
                      child: Image(
                        fit: BoxFit.fill,
                        image: NetworkImage(_image),
                      ),
                    ),),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: myDetailsContainer1(restaurantName),
                    ),
                  ),

                ],)
          ),
        ),
      ),
    );
  }

  Widget myDetailsContainer1(String restaurantName) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Container(
              child: Text(restaurantName,
                style: TextStyle(
                    color: Colors.teal,
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold),
              )),
        ),
        SizedBox(height:5.0),
        Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                    child: Text(
                      "4.1",
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 18.0,
                      ),
                    )),
                Container(
                  child: Icon(
                    FontAwesomeIcons.solidStar,
                    color: Colors.amber,
                    size: 15.0,
                  ),
                ),
                Container(
                  child: Icon(
                    FontAwesomeIcons.solidStar,
                    color: Colors.amber,
                    size: 15.0,
                  ),
                ),
                Container(
                  child: Icon(
                    FontAwesomeIcons.solidStar,
                    color: Colors.amber,
                    size: 15.0,
                  ),
                ),
                Container(
                  child: Icon(
                    FontAwesomeIcons.solidStar,
                    color: Colors.amber,
                    size: 15.0,
                  ),
                ),
                Container(
                  child: Icon(
                    FontAwesomeIcons.solidStarHalf,
                    color: Colors.amber,
                    size: 15.0,
                  ),
                ),
                Container(
                    child: Text(
                      "(946)",
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 18.0,
                      ),
                    )),
              ],
            )),
        SizedBox(height:5.0),
        Container(
            child: Text(
              "TURGUTLU BELEDİYESİ",
              style: TextStyle(
                color: Colors.black54,
                fontSize: 18.0,
              ),
            )),
        SizedBox(height:5.0),
        Container(
            child: Text(
              "TEL: 0236 313 27 27",
              style: TextStyle(
                  color: Colors.black54,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold),
            )),
      ],
    );
  }

  Widget _buildGoogleMap(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition:  CameraPosition(target: LatLng(38.4962231, 27.7033611), zoom: 15), //harita acildiginda baslangic olarak gosterilecek konum
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        markers: {
          goldenTowersMarker,alpartAvmMarker,universiteMarker,heykellerMarker,ortaParkMarker,ucakTaksi,yeniMarker1,yeniMarker2,yeniMarker3,yeniMarker4,yeniMarker4,yeniMarker5,yeniMarker6,yeniMarker7,yeniMarker8,yeniMarker9,yeniMarker10,yeniMarker11,yeniMarker12,yeniMarker13,yeniMarker14,yeniMarker15,yeniMarker16,yeniMarker17,yeniMarker18,yeniMarker19
        },
      ),
    );
  }

  Future<void> _gotoLocation(double lat,double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(lat, long), zoom: 19,tilt: 50.0,
      bearing: 45.0,)));
  }
}

Marker heykellerMarker = Marker(
  markerId: MarkerId('heykeller'),
  position: LatLng(38.492658, 27.7028519),
  infoWindow: InfoWindow(title: 'Heykeller'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);

Marker ortaParkMarker = Marker(
  markerId: MarkerId('ortaPark'),
  position: LatLng(38.496595, 27.706007),
  infoWindow: InfoWindow(title: 'Orta Park'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker ucakTaksi = Marker(
  markerId: MarkerId('ucakTaksi'),
  position: LatLng(38.4891819,27.7010217),
  infoWindow: InfoWindow(title: 'Uçak Taksi'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);



Marker goldenTowersMarker = Marker(
  markerId: MarkerId('goldenTowers'),
  position: LatLng(38.486626, 27.698338),
  infoWindow: InfoWindow(title: 'Golden Towers'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker alpartAvmMarker = Marker(
  markerId: MarkerId('alpartAvm'),
  position: LatLng(38.4905648, 27.7073549),
  infoWindow: InfoWindow(title: 'Alphart AVM'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker universiteMarker = Marker(
  markerId: MarkerId('universite'),
  position: LatLng(38.492227, 27.706980),
  infoWindow: InfoWindow(title: 'Universite'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);

//24 yeni ekelecekler

Marker yeniMarker1 = Marker(
  markerId: MarkerId('Misal Sitesi'),
  position: LatLng(38.487024, 27.696922),
  infoWindow: InfoWindow(title: 'Misal Sitesi'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker2 = Marker(
  markerId: MarkerId('Sağlık Ocağı'),
  position: LatLng(38.486086, 27.703955),
  infoWindow: InfoWindow(title: 'Sağlık Ocağı'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker3 = Marker(
  markerId: MarkerId('Irmak Sokak'),
  position: LatLng(38.4838843, 27.7008687),
  infoWindow: InfoWindow(title: 'Irmak Sokak'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker4 = Marker(
  markerId: MarkerId('Irlamaz Yolu'),
  position: LatLng(38.4828146, 27.7012549),
  infoWindow: InfoWindow(title: 'Irlamaz Yolu'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker5 = Marker(
  markerId: MarkerId('Mimar Sinan Cad'),
  position: LatLng(38.488994, 27.701037),
  infoWindow: InfoWindow(title: 'Mimar Sinan Cad'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker6 = Marker(
  markerId: MarkerId('Yunus Emre Cami'),
  position: LatLng(38.489473, 27.726865),
  infoWindow: InfoWindow(title: 'Yunus Emre Cami'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker7 = Marker(
  markerId: MarkerId('Fatih Caddesi'),
  position: LatLng(38.490299, 27.728893),
  infoWindow: InfoWindow(title: 'Fatih Caddesi'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker8 = Marker(
  markerId: MarkerId('Hüdavendigar'),
  position: LatLng(38.487878, 27.725900),
  infoWindow: InfoWindow(title: 'Hüdavendigar'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker9 = Marker(
  markerId: MarkerId('Bilgikent Okulları'),
  position: LatLng(38.490771, 27.735469),
  infoWindow: InfoWindow(title: 'Bilgikent Okulları'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker10 = Marker(
  markerId: MarkerId('Ergenekon Sağlık Ocağı'),
  position: LatLng(38.490530, 27.73019),
  infoWindow: InfoWindow(title: 'Ergenekon Sağlık Ocağı'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker11 = Marker(
  markerId: MarkerId('Arif Canpoyraz İlkokulu'),
  position: LatLng(38.491953, 27.724832),
  infoWindow: InfoWindow(title: 'Arif Canpoyraz İlkokulu'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker12 = Marker(
  markerId: MarkerId('Besime Elagöz Ortaokulu'),
  position: LatLng(38.492283, 27.72868),
  infoWindow: InfoWindow(title: 'Besime Elagöz Ortaokulu'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker13 = Marker(
  markerId: MarkerId('Saklı Cennet'),
  position: LatLng(38.49254, 27.728524),
  infoWindow: InfoWindow(title: 'Saklı Cennet'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker14 = Marker(
  markerId: MarkerId('Saray Park'),
  position: LatLng(38.494855, 27.724967),
  infoWindow: InfoWindow(title: 'Saray Park'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker15 = Marker(
  markerId: MarkerId('Dereköy Cami'),
  position: LatLng(38.50038, 27.7064471),
  infoWindow: InfoWindow(title: 'Dereköy Cami'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker16 = Marker(
  markerId: MarkerId('Turgutlu Ketenci Sitesi'),
  position: LatLng(38.493992, 27.726831),
  infoWindow: InfoWindow(title: 'Turgutlu Ketenci Sitesi'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker17 = Marker(
  markerId: MarkerId('Şehitler Cami'),
  position: LatLng(38.495367, 27.718504),
  infoWindow: InfoWindow(title: 'Şehitler Cami'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker18 = Marker(
  markerId: MarkerId('Yalçın Sokak'),
  position: LatLng(38.4967, 27.711737),
  infoWindow: InfoWindow(title: 'Yalçın Sokak'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);
Marker yeniMarker19 = Marker(
  markerId: MarkerId('Sedat Özcan Öğrenci Yurdu'),
  position: LatLng(38.500042, 27.709743),
  infoWindow: InfoWindow(title: 'Sedat Özcan Öğrenci Yurdu'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueViolet,
  ),
);