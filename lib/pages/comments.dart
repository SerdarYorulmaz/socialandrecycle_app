import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free/pages/home.dart';
import 'package:free/widgets/header.dart';
import 'package:free/widgets/progress.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:timeago/timeago.dart';

class Comments extends StatefulWidget {
  final String postId;
  final String postOwnerId;
  final String postMediaUrl;

  Comments({this.postId, this.postOwnerId, this.postMediaUrl});

  @override
  CommentsState createState() => CommentsState(
        postId: this.postId,
        postOwnerId: this.postOwnerId,
        postMediaUrl: this.postMediaUrl,
      );
}

class CommentsState extends State<Comments> {
  final String postId;
  final String postOwnerId;
  final String postMediaUrl;
  TextEditingController commentController =
      TextEditingController(); //text te yazilan yazilari algilamak icin

  bool _validate = false;

  CommentsState({this.postId, this.postOwnerId, this.postMediaUrl});

  buildComments() {
    //asenkron veri icin
    return StreamBuilder(
        //async islemlerde kullanilir. async  veri kaydetme işlemlerinde kullanilir.
        stream: commentsRef
            .document(postId)
            .collection('comments')
            .orderBy("timestamp", descending: false)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            //data henuz gelmediyse progress goster
            return circularProgress();
          }
          List<Comment> comments = []; //data geldiyse comment list tine ekle
          snapshot.data.documents.forEach((doc) {
            comments.add(Comment.fromDocument(doc));
          });
          return ListView(children: comments);
        });
  }

  addComment() {
    setState(() {
      commentController.text.isEmpty ? _validate = true : _validate = false;
    });
    if (_validate==false) {
      //firestore kayit icin
      commentsRef
          .document(postId) //postId gore yorumu firestore kaydedecegiz
          .collection("comments")
          .add({
        "username": currentUser.username,
        "comment": commentController.text,
        //comment alan  texti firebase deki comment alanina kaydediyoruz
        "timestamp": timestamp,
        "avatarUrl": currentUser.photoUrl,
        "userId": currentUser.id,
      });
      bool isNotPostOwner = postOwnerId != currentUser.id;
      if (isNotPostOwner) {
        activityFeedRef // comment eklendiginde dair aktiviti ekleme
            .document(postOwnerId)
            .collection('feedItems')
            .add({
          "type": "comment",
          "commentData": commentController.text,
          "timestamp": timestamp,
          "postId": postId,
          "userId": currentUser.id,
          "username": currentUser.username,
          "userProfileImg": currentUser.photoUrl,
          "mediaUrl": postMediaUrl,
        });
      }

      commentController.clear();
    }else{
    print("islem basarısız");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: header(context, titleText: "Yorumlar"),
      body: Column(
        children: <Widget>[
          Expanded(
            child: buildComments(),
          ),
          Divider(),
          ListTile(
            //commentin en alt kismi
            title: TextFormField(
              controller: commentController,
              //yukrda tanimladigimiz text algilama sinifi burda yaziyoruz
              decoration: InputDecoration(
                labelText: "Yorum Yaz..",
                errorText: _validate ? 'Value Can\'t Be Empty' : null,
              ),
            ),
            trailing: OutlineButton(
              //saydam bir dugme
              onPressed: addComment,
              borderSide: BorderSide.none,
              child: Text("Paylaş"),
            ),
          ),
        ],
      ),
    );
  }
}

class Comment extends StatelessWidget {
  final String username;
  final String userId;
  final String avatarUrl;
  final String comment;
  final Timestamp timestamp;

  Comment({
    this.username,
    this.userId,
    this.avatarUrl,
    this.comment,
    this.timestamp, //yorum atildigi zaman icin onemli 12 dk once gibi
  });

  factory Comment.fromDocument(DocumentSnapshot doc) {
    return Comment(
      username: doc['username'],
      userId: doc['userId'],
      comment: doc['comment'],
      timestamp: doc['timestamp'],
      avatarUrl: doc['avatarUrl'],
    );
  }

  @override
  Widget build(BuildContext context) {
    timeago.setLocaleMessages("tr", TrMessages());
    return Column(
      //kullanicinin goruntu kismi yorum icin
      children: <Widget>[
        ListTile(
          title: Text(comment),
          leading: CircleAvatar(
            backgroundImage: CachedNetworkImageProvider(avatarUrl),
          ),

          subtitle: Text(timeago.format(timestamp.toDate(),locale: "tr")),
        ),
        Divider(),
      ],
    );
  }
}
