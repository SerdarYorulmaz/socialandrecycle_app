import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import "package:flutter/material.dart";
import 'package:free/models/user.dart';
import 'package:free/pages/home.dart';
import 'package:free/widgets/progress.dart';

class EditProfile extends StatefulWidget {
  final String currentUserId;

  EditProfile({this.currentUserId});

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Textlerde yapilan degisikligi algilamak icin
  TextEditingController displayNameController = TextEditingController();
  TextEditingController bioController = TextEditingController();

  bool isLoading = false;
  User user;
  bool _displayNameValid = true;
  bool _bioValid = true;

  @override
  void initState() {
    super.initState();
    getUser();
  }

  getUser() async {
    setState(() {
      isLoading = true;
    });
    DocumentSnapshot doc = await usersRef.document(widget.currentUserId).get();
    user = User.fromDocument(doc);

    displayNameController.text = user.displayName;
    bioController.text = user.bio;

    setState(() {
      isLoading = false;
    });
  }

  //display Textbox tasarimi
  Column buildDisplayNameField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      //baslangis olarak x:0 basla gibi anlaminda
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
          child: Text(
            "Kullanıcı Adı",
            style: TextStyle(color: Colors.grey),
          ),
        ),
        TextField(
          controller: displayNameController,
          decoration: InputDecoration(
            hintText: "Kullanıcı Adı Güncelle",
            errorText: _displayNameValid ? null : "Kullanıcı ismi çok kısa",
          ),
        ),
      ],
    );
  }

//display bio tasarimi
  Column buildBioField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      //baslangis olarak x:0 basla gibi anlaminda
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
          child: Text(
            "Açıklama",
            style: TextStyle(color: Colors.grey),
          ),
        ),
        TextField(
          controller: bioController,
          decoration: InputDecoration(
            hintText: "Açıklama Güncelle",
            errorText: _bioValid ? null : "Açıklama çok uzun",
          ),
        ),
      ],
    );
  }



  //tasarim kismi
// key: _scaffoldKey  snack bar icin gerekli
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey, //global key
      appBar: AppBar(
        title: Text(
          //profile kisminin ust kismi
          "Profil Düzenle",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () => Navigator.pop(context), //bir onceki sayfa
            icon: Icon(
              Icons.done,
              size: 30.0,
              color: Colors.white,
            ),

          ),

        ],
        centerTitle: true,
        backgroundColor: Theme.of(context).accentColor,
      ),
      body: isLoading
          ? circularProgress()
          : ListView(
              //veri gelinceye kadar yukleniyor isaretini goster
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 16.0, bottom: 8.0),
                        child: CircleAvatar(
                          //avatar olan kisim
                          radius: 50.0,
                          backgroundImage:
                              CachedNetworkImageProvider(user.photoUrl),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Column(
                          children: <Widget>[
                            buildDisplayNameField(),
                            buildBioField(),
                          ],
                        ),
                      ),
                      RaisedButton(
                        onPressed: () => updateProfileData(),
                        child: Text(
                          "Profili Güncelle",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
//                      Padding(
//                        padding: EdgeInsets.all(16.0),
//                        child: FlatButton.icon(
//                          onPressed: () => logout(),
//                          icon: Icon(
//                            Icons.cancel,
//                            color: Colors.red,
//                          ),
//                          label: Text(
//                            "logout",
//                            style: TextStyle(color: Colors.red, fontSize: 20.0),
//                          ),
//                        ),
//                      ),
                    ],
                  ),
                ),
              ],
            ),
    );
  }

  updateProfileData() {
    //butona basildiginda display textinin validation controlleri yapiyoruz
    print("SELAM SERDARCIK");
    setState(() {
      displayNameController.text.trim().length < 3 ||
          displayNameController.text.isEmpty
          ? _displayNameValid = false
          : _displayNameValid = true;
      bioController.text.trim().length > 100
          ? _bioValid = false
          : _bioValid = true;
    });
    print("SELAM SERDARCIKControl");
    if (_displayNameValid && _bioValid) {
      // validationlardan gecerse firebase update islemi yapiyoruz
      usersRef.document(widget.currentUserId).updateData({
        "displayName": displayNameController.text,
        "bio": bioController.text,
      });
      print("SELAM SERDARCIKControlicinde");

      SnackBar snackbar = SnackBar(
        content: Text("Profile Updated!!"),
      );

      _scaffoldKey.currentState.showSnackBar(snackbar);
    }

    print("BAYYY SERDARCIK");
  }

//  logout() async{
//    await googleSignIn.signOut();
//    Navigator.push(context, MaterialPageRoute(builder: (context)=>Home()));
//  }


}
