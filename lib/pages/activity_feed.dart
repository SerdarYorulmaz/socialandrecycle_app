import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:free/pages/home.dart';
import 'package:free/pages/post_screen.dart';
import 'package:free/pages/profile.dart';
import 'package:free/widgets/header.dart';
import 'package:free/widgets/post_tile.dart';
import 'package:free/widgets/progress.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:timeago/timeago.dart';

class ActivityFeed extends StatefulWidget {
  @override
  _ActivityFeedState createState() => _ActivityFeedState();
}

class _ActivityFeedState extends State<ActivityFeed> {
  getActivityFeed() async {
    QuerySnapshot snapshot = await activityFeedRef
        .document(currentUser.id)
        .collection('feedItems')
        .orderBy('timestamp', descending: true)
        .limit(50)
        .getDocuments();
    List<ActivityFeedItem> feedItems = [];
    snapshot.documents.forEach((doc) {
      feedItems.add(ActivityFeedItem.fromDocument(doc));
      // print('Activity Feed Item:${doc.data}');
    });
    return feedItems;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //tasarim kismi
      backgroundColor: Colors.tealAccent,

      appBar: header(context, titleText: "Bildirimler"),
      body: Container(
        child: FutureBuilder(
          future: getActivityFeed(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              //data daha yuklenmediyse
              return circularProgress();
            }
            return ListView(
              children: snapshot.data,
            );
          },
        ),
      ),
    );
  }
}

Widget mediaPreview;
String activityItemText;

class ActivityFeedItem extends StatelessWidget {
  final String username;
  final String userId;
  final String type; // like,follow yada comment olabilir
  final String mediaUrl;
  final String postId;
  final String userProfileImg;
  final String commentData;
  final Timestamp timestamp;

  ActivityFeedItem({
    this.username,
    this.userId,
    this.type, // like,follow yada comment olabilir
    this.mediaUrl,
    this.postId,
    this.userProfileImg,
    this.commentData,
    this.timestamp,
  });

  factory ActivityFeedItem.fromDocument(DocumentSnapshot doc) {
    return ActivityFeedItem(
      username: doc['username'],
      userId: doc['userId'],
      type: doc['type'],
      postId: doc['postId'],
      userProfileImg: doc['userProfileImg'],
      commentData: doc['commentData'],
      timestamp: doc['timestamp'],
      mediaUrl: doc['mediaUrl'],
    );
  }

  showPost(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PostScreen(
          postId: postId,
          userId: userId,
        ),
      ),
    );
  }

  configureMediaPreview(context) {
    //Tasarim kismi
    if (type == "like" || type == 'comment') {
      Padding(
        padding: const EdgeInsets.all(8.0),
        child:
            Container(
             child: mediaPreview = GestureDetector(
                onTap: () => Text(""),
                child: Container(
                  height: 50.0,
                  width: 50.0,
                  child: AspectRatio(
                    aspectRatio: 16 / 9,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: CachedNetworkImageProvider(mediaUrl),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),

      );
    } else {
      mediaPreview = Text("");
    }

    if (type == 'like') {
      activityItemText = "paylaşımı beğendi";
    } else if (type == 'follow') {
      activityItemText = "seni takip etti";
    } else if (type == 'comment') {
      activityItemText = 'yorumu: $commentData';
    } else {
      activityItemText = "Hata bilinmeyen bir type '$type' ";
    }
  }

  @override
  Widget build(BuildContext context) {
    configureMediaPreview(context);
    timeago.setLocaleMessages("tr", TrMessages());
    return Padding(
      padding: EdgeInsets.only(bottom: 2.0),
      child: Container(
        color: Colors.white54,
        child: ListTile(
          title: GestureDetector(
            onTap: () => showProfile(context, profileId: userId),
            child: RichText(
              overflow: TextOverflow.ellipsis,
              text: TextSpan(
                style: TextStyle(
                  fontSize: 14.0,
                  color: Colors.black,
                ),
                children: [
                  TextSpan(
                    text: username,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                    text: ' $activityItemText',
                  ),
                ],
              ),
            ),
          ),
          leading: CircleAvatar(
            backgroundImage: CachedNetworkImageProvider(userProfileImg),
          ),
          subtitle: Text(

            timeago.format(timestamp.toDate(),locale: "tr"),
            overflow: TextOverflow.ellipsis,
          ),
          trailing: GestureDetector(
              onTap: () => Text(""),
              child:mediaPreview
                  ),


        ),
      ),
    );
  }
}

showProfile(BuildContext context, {String profileId}) {
  Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => Profile(
        profileId: profileId,
      ),
    ),
  );
}
