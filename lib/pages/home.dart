import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:free/models/user.dart';
import 'package:free/pages/activity_feed.dart';
import 'package:free/pages/chat_screen.dart';
import 'package:free/pages/create_account.dart';
import 'package:free/pages/profile.dart';
import 'package:free/pages/search.dart';
import 'package:free/pages/search2.dart';
import 'package:free/pages/timeline.dart';
import 'package:free/pages/upload.dart';
import 'package:google_sign_in/google_sign_in.dart';

//firebase baglanti islemleri ve firebase koleksiyon olusturma
final GoogleSignIn googleSignIn = GoogleSignIn();
final StorageReference storageRef = FirebaseStorage.instance.ref();
final usersRef = Firestore.instance.collection('users');
final postsRef = Firestore.instance.collection('posts');
final commentsRef = Firestore.instance
    .collection('comments'); //yeni bir dal olusturduk commentsler icin
final activityFeedRef = Firestore.instance.collection('feed');
final followersRef = Firestore.instance.collection('followers');
final followingRef = Firestore.instance.collection('following');
final timelineRef = Firestore.instance.collection('timeline');

final DateTime timestamp = DateTime.now();
User currentUser;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  bool isAuth = false;
  PageController pageController;
  int pageIndex = 0;

  @override
  void initState() {
    super.initState();
    pageController = PageController();

    //kullanici oturum actigini burdan anlariz
    googleSignIn.onCurrentUserChanged.listen((account) {
      handleSignIn(account);
    }, onError: (err) {
      print('Error signin in: $err');
    });
    //kullanici dogrulama
    googleSignIn.signInSilently(suppressErrors: false).then((account) {
      handleSignIn(account);
    }).catchError((err) {
      print('Error signing in: $err');
    });
  }

  handleSignIn(GoogleSignInAccount account) async {
    if (account != null) {
      await createUserInFirestore();
      setState(() {
        isAuth = true;
      });
      configurePushNotifications(); //push bildirimleri
    } else {
      setState(() {
        isAuth = false;
      });
    }
  }

  configurePushNotifications() {
    final GoogleSignInAccount user = googleSignIn.currentUser;
    if (Platform.isIOS) getiOSPermission();

    _firebaseMessaging.getToken().then((token){
      print("firebase mesajlaşma token: $token \n");
      usersRef
        .document(user.id)
        .updateData({"androidNotificationToken":token});

    });

    _firebaseMessaging.configure(
      //onLaunch: (Map <String,dynamic> message) async {},

      //onResume:(Map <String,dynamic> message) async {},
      onMessage: (Map <String,dynamic> message) async {
        print("on message: $message \n");
        final String recipientId=message['data']['recipient'];
        final String body=message['notification']['body'];

        if(recipientId==user.id){
          print("gösterilen bildirimler");
          SnackBar snackBar=SnackBar(content: Text(body,overflow: TextOverflow.ellipsis,));
          _scaffoldKey.currentState.showSnackBar(snackBar);

        }
        print("Gösterilmeyen bilgiler");

      },
    );
  }

  getiOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(alert: true, badge: true, sound: true));
    _firebaseMessaging.onIosSettingsRegistered.listen((settings) {
      print("ayarlar kaydedildi: $settings");
    });
  }

  createUserInFirestore() async {
    // kimliklere gore  user collection olup olmadigin kontrol eder
    // hesap yoksa hesap olustur

    final GoogleSignInAccount user = googleSignIn.currentUser;
    DocumentSnapshot doc = await usersRef.document(user.id).get();

    if (!doc.exists) {
      final username = await Navigator.push(
          context, MaterialPageRoute(builder: (context) => CreateAccount()));

      usersRef.document(user.id).setData({
        // hesap yoksa hesap olustur
        "id": user.id,
        "username": username,
        "photoUrl": user.photoUrl,
        "email": user.email,
        "displayName": user.displayName,
        "bio": "",
        "timestamp": timestamp,
      });
      //kullanici takipci yap ve timeline ekle
      await followersRef
          .document(user.id)
          .collection('userFollowers')
          .document(user.id)
          .setData({});

      doc = await usersRef.document(user.id).get();
    }
    currentUser = User.fromDocument(doc);
    print('userrrr ${currentUser}');
    print('userrrrnameeee ${currentUser.username}');
  }

  @override
  void dispose() {
    //sayfa degisklik olmayacaksa
    pageController.dispose();
    super.dispose();
  }

  login() {
    googleSignIn.signIn();
  }

  logout() {
    googleSignIn.signOut();
  }

  onPageChanged(int pageIndex) {
    setState(() {
      this.pageIndex = pageIndex;
    });
  }

  onTap(int pageIndex) {
    pageController.animateToPage(
      pageIndex,
      duration: Duration(milliseconds: 300),
      curve: Curves.easeInOut, //animasyonlu gecis(slider gibi)
    );
  }

  Scaffold buildAuthScreen() {
//    return RaisedButton(
//      child: Text("Logout"),
//      onPressed: logout,
//    );

    return Scaffold(
      key: _scaffoldKey,
      body: PageView(
        children: <Widget>[
          Timeline(currentUser: currentUser),
          ActivityFeed(),
          Upload(currentUser: currentUser),
          Search2(),

          ChatScreen(currentUser: currentUser),
          Profile(profileId: currentUser?.id),
        ],
        controller: pageController,
        onPageChanged: onPageChanged,
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: CupertinoTabBar(
        currentIndex: pageIndex,
        onTap: onTap,
        activeColor: Theme.of(context).primaryColor,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.whatshot)),
          BottomNavigationBarItem(icon: Icon(Icons.notifications_active)),
          BottomNavigationBarItem(
              icon: Icon(
            Icons.photo_camera,
            size: 35.0,
          )),
          BottomNavigationBarItem(icon: Icon(Icons.search)),

          BottomNavigationBarItem(icon: Icon(Icons.email)),
          BottomNavigationBarItem(icon: Icon(Icons.account_circle)),
        ],
      ),
    );
  }

  Scaffold buildUnAuthScreen() {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Theme.of(context).primaryColor, //head kismi icin
              Theme.of(context).accentColor.withOpacity(0.4) //body kismi icin
            ], //maindeki themalardan kullaniliyor(kalitim)
          ),
        ),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'KullanTekrarKazan',
              style: TextStyle(
                  fontSize: 60.0, fontFamily: "Signatra", color: Colors.white),
            ),
            GestureDetector(
              onTap: () => login(),
              child: Container(
                width: 260.0,
                height: 60.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image:
                          AssetImage('assets/images/google_signin_button.png'),
                      fit: BoxFit.cover),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return isAuth ? buildAuthScreen() : buildUnAuthScreen();
  }
}
