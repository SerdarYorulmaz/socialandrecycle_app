const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
exports.onCreateFollower = functions.firestore
    .document("/followers/{userId}/userFollowers/{followerId}")
    .onCreate(async (snapshot, context) => {
        console.log("Follower Created", snapshot.id);
        const userId = context.params.userId;
        const followerId = context.params.followerId;

        //kullanicilarin paylasimlarini takip et

        const followedUserPostsRef = admin
            .firestore()
            .collection('posts')
            .doc(userId)
            .collection('userPosts');

        //kullanicinin timeline ni takip et

        const timelinePostsRef = admin
            .firestore()
            .collection('timeline')
            .doc(followerId)
            .collection('timelinePosts');

            //takip ettigin kullanicilarin postlarini getir
        const querySnapshot = await followedUserPostsRef.get();

        //takip ettigin kullanicinin gönderilerini timeline ekle

        querySnapshot.forEach(doc => {
            if (doc.exists) {
                const postId = doc.id;
                const postData = doc.data();
                timelinePostsRef.doc(postId).set(postData);
            }
        });
    });

exports.onDeleteFollower = functions.firestore
    .document("/followers/{userId}/userFollowers/{followerId}")
    .onDelete(async (snapshot, context) => {
        console.log("Follower Deleted", snapshot.id);


        const userId = context.params.userId;
        const followerId = context.params.followerId;

        const timelinePostsRef = admin
            .firestore()
            .collection('timeline')
            .doc(followerId)
            .collection('timelinePosts')
            .where("ownerId", "==", userId);

        const querySnapshot = await timelinePostsRef.get();
        querySnapshot.forEach(doc => {
            if (doc.exists) {
                doc.ref.delete();
            }
        });
    });

//post paylastiginda takipcilerin timeline ekranında  postu ekle
exports.onCreatePost = functions.firestore
    .document('/posts/{userid}/userPosts/{postId}')
    .onCreate(async (snapshot, context) => {
        const postCreated = snapshot.data();
        const userId = context.params.userId;
        const postId = context.params.postId;

        //kullanicinin tum takipcilerini getir

        const userFollowersRef = admin.firestore()
            .collection('followers')
            .doc(userId)
            .collection('userFollowers');

        const querySnapshot = await userFollowersRef.get();
        //her takipcinin timeline yeni post ekle
        querySnapshot.forEach(doc => {

            const followerId = doc.id;

            admin
                .firestore()
                .collection('timeline')
                .doc(followerId)
                .collection('timelinePosts')
                .doc(postId)
                .set(postCreated);

        });

    });

exports.onUpdatePost = functions.firestore
    .document('/posts/{userId}/userPosts/{postId}')
    .onUpdate(async (change, context) => {
        const postUpdated = change.after.data();
        const userId = context.params.userId;
        const postId = context.params.postId;

        //kullanicinin tum takipcilerini getir

        const userFollowersRef = admin.firestore()
            .collection('followers')
            .doc(userId)
            .collection('userFollowers');
        // timeline guncelle
        const querySnapshot = await userFollowersRef.get();

        querySnapshot.forEach(doc => {

            const followerId = doc.id;

            admin
                .firestore()
                .collection('timeline')
                .doc(followerId)
                .collection('timelinePosts')
                .doc(postId)
                .get().then(doc => {
                    if (doc.exists) {
                        doc.ref.update(postUpdated);
                    }
                });

        });
    });

exports.onDeletePost = functions.firestore
    .document('/posts/{userId}/userPosts/{postId}')
    .onDelete(async (snapshot, context) => {


        const userId = context.params.userId;
        const postId = context.params.postId;

        //kullanicinin tum takipcilerini getir

        const userFollowersRef = admin.firestore()
            .collection('followers')
            .doc(userId)
            .collection('userFollowers');
        // timeline guncelle
        const querySnapshot = await userFollowersRef.get();
        //paylasimi sildiginde diger timeline deki paylasimlarida sil
        querySnapshot.forEach(doc => {

            const followerId = doc.id;

            admin
                .firestore()
                .collection('timeline')
                .doc(followerId)
                .collection('timelinePosts')
                .doc(postId)
                .get().then(doc => {
                    if (doc.exists) {
                        doc.ref.delete();
                    }
                });

        });

    });

exports.onCreateActivityFeedItem = functions.firestore
    .document('/feed/{userId}/feedItems/{activityFeedItem}')
    .onCreate(async (snapshot, context) => {
        console.log('Activity ögesi olusturuldu', snapshot.data());

        //kullanicinin aktivitelere  baglanmasini sagla
        const userId = context.params.userId;
        const userRef = admin.firestore().doc(`users/${userId}`);
        const doc = await userRef.get();
        //bir kullanici bildirim olup olmadigini token ile kontrol eder
        //token varsa bildirim gonderir
        const androidNotificationToken = doc.data().androidNotificationToken;
        const createdActivityFeedItem = snapshot.data();
        if (androidNotificationToken) {
            //Bildirim Gönder
            sendNotification(androidNotificationToken, createdActivityFeedItem);
        } else {
            console.log("kullanici icin token yoksa bildirim gonderemez");
        }

        function sendNotification(androidNotificationToken, activityFeedItem) {
            let body;

            //bildirim turune gore body degerini degistir
            switch (activityFeedItem.type) {
                case "comment":
                    body = `${activityFeedItem.username} replied: ${activityFeedItem.commentData}`;
                    break;

                case "like":
                    body = `${activityFeedItem.username} liked your post`;
                    break;


                case "follow":
                    body = `${activityFeedItem.username} started following you `;
                    break;

                default:
                    break;


            }
            // push bildirimi icin  mesaj oluştur
            const message = {
                notification: { body },
                token: androidNotificationToken,
                data: { recipient: userId }
            };

            //mesaj gonder

            admin
                .messaging()
                .send(message)
                .then(response => {
                    //donus degeri kimlik dizisi
                    console.log("mesaj gonderme islemi basarili", response);
                })
                .catch(error => {
                    console.log("Error  mesajini gonder", error);
                })
        }

    });